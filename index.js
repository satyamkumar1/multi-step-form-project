
const backBtn = document.querySelector('.back-btn');
const nextBtn = document.querySelector('.next-btn');



const stepBox1 = document.querySelector('.step-box1');
const stepBox2 = document.querySelector('.step-box2');
const stepBox3 = document.querySelector('.step-box3');
const stepBox4 = document.querySelector('.step-box4');

stepBox1.style.backgroundColor = "pink";


const errorMessages1 = document.querySelector('.error1');
const errorMessages2 = document.querySelector('.error2');
const errorMessages3 = document.querySelector('.error3');



backBtn.style.display = 'none';

let arr = [];

let counter = 0;

backBtn.addEventListener('click', backpage = () => {
    --counter;
    arr.length = 0;
    if (counter == 0) {
        stepBox2.style.backgroundColor = "";
        stepBox1.style.backgroundColor = "pink";


        document.querySelector('.personal-info').style.display = 'flex';
        document.querySelector('.your-plan').style.display = 'none';


    }
    if (counter == 1) {


        page1();
        stepBox3.style.backgroundColor = "";

        document.querySelector('.pick-add-plan').style.display = 'none';



    }
    if (counter == 2) {
        page2();
        stepBox4.style.backgroundColor = "";

        document.querySelector('.finishing-plan').style.display = 'none';

    }
    if (counter == 3) {
        page3()

        document.querySelector('.pick-add-plan').style.display = 'none';



    }




})




nextBtn.addEventListener('click', () => {


    const valid = formvalidation();

    if (valid) {
        counter++;
        console.log(counter);
        page1();

        choosePlan();
        if (counter == 2) {
            page2();
            pickandplanDetail();
        }
        if (counter == 3) {

            page3()
            finishingdetail()
        }
        if (nextBtn.textContent == 'confirm' && counter == 4) {
            page4()


        }


    }

})



function page1() {
    document.querySelector('.personal-info').style.display = 'none';
    document.querySelector('.your-plan').style.display = 'block';
    stepBox1.style.backgroundColor = "";
    stepBox2.style.backgroundColor = "pink";
    backBtn.style.display = 'block';



}
function page2() {

    stepBox2.style.backgroundColor = "";
    stepBox3.style.backgroundColor = "pink";

    document.querySelector('.your-plan').style.display = 'none';
    document.querySelector('.pick-add-plan').style.display = 'block';


}
function page3() {
    stepBox3.style.backgroundColor = "";
    stepBox2.style.backgroundColor = "";

    stepBox4.style.backgroundColor = "pink";
    document.querySelector('.your-plan').style.display = 'none';

    document.querySelector('.pick-add-plan').style.display = 'none';
    document.querySelector('.finishing-plan').style.display = 'block';

    nextBtn.innerHTML = "confirm";



}
function page4() {

    document.querySelector('.your-plan').style.display = 'none';
    stepBox2.style.backgroundColor = "";

    document.querySelector('.finishing-plan').style.display = 'none';
    document.querySelector('.thankyou-page').style.display = 'block';

    backBtn.style.display = 'none';
    nextBtn.style.display = 'none';


}





function formvalidation() {

    const username = document.querySelector('.name').value;
    const email = document.querySelector('.email').value;
    const phone = document.querySelector('.phone').value;

    let namefield = false;
    let emailfield = false;
    let phonefield = false;


    if (!username) {
        errorMessages1.innerText = ("Name is required");
    } else if ((!/^[a-zA-Z]+$/.test(username)) || username.length < 3) {
        errorMessages1.innerText = ("Invalid input for Last Name");
    }
    else {
        namefield = true
    }

    if (!email) {
        errorMessages2.innerText = ("Email Address is required");
    }
     else if (!email.includes("@gmail.com")) {
        errorMessages2.innerText = ("Email required @gmail.com");
    }
    else {
        emailfield = true;
    }

    if (!phone) {
        errorMessages3.innerText = ("Phone is required");
    } else if (phone.length < 10) {
        errorMessages3.innerText = ("please add more number");
    }
    else {
        phonefield = true;
    }



    if (namefield == true && emailfield == true && phonefield == true) {

        return true;
    }

    return false;

   
}

let chooseYearormonth;

function choosePlan() {

    const toggle = document.getElementById('choice')

    toggle.addEventListener('input', (e) => {
        let arcadePrice, advancedprice, proPrice;
        if (toggle.value === '0') {
            arcadePrice = '$9/mo';
            advancedprice = '$12/mo';
            proPrice = '$15/mo'

        } else {
            arcadePrice = `$90/yr<br> 2month free`;
            advancedprice = `$120/yr<br> 2month free`;
            proPrice = `$150/yr<br> 2month free`;

        }

        document.getElementById('Arcade-monthly-price').innerHTML = arcadePrice
        document.getElementById('Advanced-monthly-price').innerHTML = advancedprice
        document.getElementById('pro-monthly-price').innerHTML = proPrice
    })






    document.querySelector('.plan-box-container').addEventListener('click', (event) => {

        event.target.style.border = "2px solid blue";

        console.log("paln" + event.target);
        const obj = {};
        const planname = (event.target.children[1].textContent);

        const plan = event.target.children[2].textContent;
        obj.name = planname;
        obj.price = plan;
        chooseYearormonth = plan;
        console.log("plan it " + plan);

        if(plan.includes('$')){
        arr.push(obj)
        }



    })
}




function pickandplanDetail() {

    if (chooseYearormonth.includes('yr')) {

        document.querySelector('.year-price1').textContent = '+$10/yr';
        document.querySelector('.year-price2').textContent = '+$20/yr';
        document.querySelector('.year-price3').textContent = '+$50/yr';



    }


    document.querySelector('.pick-add-plan').addEventListener('click', (event) => {

        if (event.target.classList.contains('checkbox')) {

            const obj = {};
            // console.log(event.target.parentElement.children[1].children[0].textContent);
            const name = (event.target.parentElement.children[1].children[0].textContent);

            const priceValue = event.target.parentElement.children[2].textContent;

            obj.name = name;
            obj.price = priceValue;

            arr.push(obj)




        }
        console.log(arr);


    })
}
let flag = false;
document.querySelector('.change-btn').addEventListener('click', () => {


    let monYearPirce = arr[0].price.slice(1).split('/')[1];
    let time;
    if (monYearPirce == 'mo') {
        time = 'monthly';
    }
    else {
        time = 'yearly'
    }
    document.querySelector('.arcade-monthly').children[0].textContent = `${arr[0].name}(${time})`;
    document.querySelector('.item-row').children[1].textContent = (arr[0].price).split(" ")[0];

    console.log("price" + arr[0].price)
    for (let i = 0; i < arr.length; i++) {
        if (monYearPirce == 'mo') {
            let totalprice = arr[i].price.split('/');
            arr[i].price = totalprice[0] + 0 + '/yr';
        }
        else {
            let totalprice = arr[i].price.split('/');
            arr[i].price = totalprice[0].slice(0, totalprice[0].length - 1) + '/mo';
        }


    }

    console.log("new arr" + arr);
    document.querySelector('#row').innerHTML = " ";
    document.querySelector(".total-price").children[1].textContent = " ";

    flag = true;
    finishingdetail();

    // console.log(arr[0].price.slice(1).split('/')[0]);







})

function finishingdetail() {

    let monYearPirce = arr[0].price.slice(1).split('/')[1];
    let time;
    if (monYearPirce == 'mo') {
        time = 'monthly';
    }
    else {
        time = 'yearly'
    }
    document.querySelector('.arcade-monthly').children[0].textContent = `${arr[0].name}(${time})`;
    document.querySelector('.item-row').children[1].textContent = (arr[0].price).split(" ")[0];

    console.log("price" + arr[0].price)

    let totalprice = Number(arr[0].price.slice(1).split('/')[0]);
    console.log(Number(arr[0].price.slice(1).split('/')[0]));

    for (let index = 1; index < arr.length; index++) {

        if (flag) {
            console.log("falg" + arr[index].price.slice(3).split('/')[0]);
            totalprice += Number(arr[index].price.slice(3).split('/')[0]);
        }
        if (time == "yearly" && !flag) {
            totalprice += Number(arr[index].price.slice(2).split('/')[0]);

        }
        if (time == "monthly" && !flag) {
            totalprice += Number(arr[index].price.slice(3).split('/')[0]);
        }
        // console.log("num" + (arr[index].price.slice(2).split('/')));
        let divData = `<p class="price-line">${arr[index].name}<span>${arr[index].price}<span></p>`

        document.querySelector('#row').innerHTML += divData;

    }
    console.log(totalprice);

    document.querySelector(".total-price").children[1].textContent = `$${totalprice}/${monYearPirce.split(" ")[0]}`;

}